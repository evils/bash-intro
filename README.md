# Preface <a name = "preface"></a>

The target audience is people new to, or considering using, unix-like operating systems like GNU/Linux, macOS and BSD.  

This is intended as the information dense support materials to a guided intro/workshop but can be used stand-alone.  
While it has been written fairly linearly, the dense nature is intended to be skimmed over.  

If you can form a question, you are encouraged to formulate a test or query a search site like [duckduckgo](https://duckduckgo.com).  
Otherwise, don't hesitate to ask for help.  

The main goal is to give you an idea of what you can do with Bash and how to do it.  
Not to teach you everything, but show you the expressiveness of Bash,  
and to give you an idea of what to search for when things don't work.  

# Table of Contents <a name = "table_of_contents"></a>
0. [Preface](#preface")
1. [ToC](#table_of_contents)  
2. [Installation](#installation)  
3. [Terminal](#terminal)  
4. [What is Bash](#what_is_bash)  
5. [Using Bash](#using_bash)  
6. [Some basic utilities](#basic_utilities)  
7. [Shortcuts](#shortcuts)  
8. [Pipes](#pipes)  
9. [Scripts](#scripts)  
10. [Globbing](#globbing)  
11. [More utilities](#more_utilities)  
	0. [String manipulation](#string_manipulation)  
	1. [Administration](#administration)  
	2. [File manipulation](#file_manipulation)  
	3. [Network management](#network_management)  
	4. [Pipe manipulation](#pipe_manipulation)  
	5. [Environment](#environment)  
	6. [Process manipulation](#process_manipulation)  
	7. [Disk manipulation](#disk_manipulation)
12. [Programs as arguments](#programs_as_arguments)  
13. [Job control](#job_control)  
14. [Command substitution](#command_substitution)  
15. [Loops and Conditionals](#loops_and_conditionals)  
16. [Customisation](#customisation)  
17. [Alternatives](#alternatives)  
18. [Package managers](#package_managers)  
19. [References](#references)  

# Installation <a name = "installation"></a>

For most unix-like operating systems, Bash should already be installed and the default shell.  
Open a terminal window and enter `echo $0`, if this does not return `bash`, enter `bash` and try again.  
If this doesn't work, call for help.  

For windows users, see if Bash on Ubuntu on windows is available for you (windows subsystem for linux).  
https://docs.microsoft.com/en-us/windows/wsl/install-win10  
https://youtu.be/Cvrqmq9A3tA (please don't watch youtube during the workshop)  

Another option would be to use a usb stick based live environment such as the one from ubuntu.  
If that is not an option, Git Bash may be viable.  
Otherwise, install and use PuTTY to connect to a linux machine via ssh (ask for help).

# Terminal <a name = "terminal"></a>

The terminal is a program that that displays program output, the shell is opened by default.  
Copying and pasting in a terminal is often done with `Ctrl+Shift+C` and `Ctrl+Shift+V`, but yours may differ.

# What is Bash <a name = "what_is_bash"></a>

Bash is a command-line interpreter, also known as a shell.  
It provides a text user interface to a computer.
It allows you to start programs and connect them to each other.  

It is installed on almost all unix-like OSes, making it an ubiquitous tool worth learning.  

A central concept in all these operating systems is the "unix philosophy" of modularity.  
Have small, purpose-built programs, (which take data in from standard input ("stdin"), perform some (fairly simple) operation,
direct their output to standard output ("stdout"))
and link them together through the shell to perform more complex actions.  

> Do one thing and do it well.  

This is the primary purpose of Bash, which is both a shell and a language for using it.

As an interactive shell, Bash is most useful for remote use via SSH where the computer isn't physically reachable (server), or does not have appropriate peripherals (raspberry pi).

Locally it is often useful for doing specific things which GUI tools are lacking, using tools lacking a GUI, or leveraging your experience from remote operations.

# Using Bash <a name = "using_bash"></a>

Bash takes input and acts on it, today we will mostly be using the interactive shell, which takes input from a terminal.  
Here, you are presented with a prompt (usually indicated by the `$` symbol) and you can simply type the name of a program to run it.  

example: `ls`

Most programs have options (A.K.A. arguments).  

`ls -a` (list all)

By convention, single letter options use a single hyphen and can have multiple options for one hyphen

`ls -lah` (long format, all, human readable)

and multi letter options use double hyphens.  

`ls --version` (show version)

This convention is not set in stone.  

## Some basic utilities <a name = "basic_utilities"></a>

	cd, cd .. 			(. is the current directory, .. is the parent dir, ../.. is 2 levels up, ~ is the home dir)
	pwd					(print working directory)
	ls					(lists the contents of the current directory)
	cat					(concatenate files and print to stdout (can take 1 file)) 
	echo				(write arguments to stdout)
	man [program name] 	(show the manual for a program)
	file [filename] 	(show information about the file)

Look around a bit, ask questions.  
By now you're probably tired of typing everything out.  

## Shortcuts <a name = "shortcuts"></a>

`^` or `C-` can mean `Ctrl`

	^+C			(exit program)
	^+D			(exit shell)
	^+A			(go to start of line)
	^+E			(go to end of line)
	^+U			(clear the entire line (works for password entry too))
	^+L			(clear the screen)
	[up arrow] 		(go back through history)
	^+R			(search back through history)
	Alt+.			(insert previous command's last parameter)
	!!			(last command, e.g.: sudo !!)
	[tab]			(tab complete)

Tab completion is the completion of any commands by unambiguous parts.  
When pressing the tab key on a partially complete command Bash will fill in what it can.  
If there is ambiguity, it will do nothing, until you press tab a second time, then you are shown the options.  

## Pipes <a name = "pipes"></a>

The `|` operator links together the stdout of the left program and the stdin of the right.  
This structure is called a pipe, and can end on any program, causing the stdout of that program to be shown on screen.  
If you want to save that output, you can redirect its output to a file with the `>` symbol (`>>` appends to a file).  
Programs can be grouped with `(parenthesis)`.  

As an example, let's do a word frequency count.  

First we `cat [filename]` followed by a pipe (`|`) to get the file into the stdin of the next program.  
Now we convert everything to lower case as to not count upper and lower case separately.  
`tr A-Z a-z` translates every character that is in the range A-Z to its equivalent position in the range a-z.  
Then we want to remove all punctuation and separate words.  
`tr --complement --squeeze-repeats a-z "\n"` translates everything that is the complement of a-z (not a character in the range a-z) to a newline and squeezes all repeated newlines into one.  
(--complement uses the first given pattern and --squeeze-repeats uses the second, which is perfect for this application)  
Now we want to sort the result of that because the program after it needs a sorted input for the behaviour we want.  
`sort`  
And then we want to count the occurrences of each unique line. (uniq (yes they saved a few letters) removes matching adjacent lines)  
`uniq --count`  
Finally, we use sort again, this time sort in reverse order, by number.  
`sort --reverse --numeric-sort`  
To save this into a file, we use the `>` operator and specify a file (this file gets created if it doesn't exist).  
`> freqcounted`  
Files in unix-like systems don't require an extension, it is only there for readability and to facilitate things like globbing. The extension is purely a part of the filename.  

`cat bash-ref.txt | tr A-Z a-z | tr --complement --squeeze-repeats a-z "\n" | sort | uniq --count | sort --reverse --numeric-sort > freqcounted`  

To run all these commands as one, we can put them into a file and have bash run that.  

# Scripts <a name = "scripts"></a>

In the spirit of modularity, one usually tries to avoid making scripts file specific, therefore we're not going to include the `cat` and `>` commands in our script.  

Make a file called "freqcount.sh" put the following in it:  
```bash
#!/usr/bin/env bash

tr A-Z a-z | tr -cs a-z "\n" | sort | uniq -c | sort -rn

```  
This is a Bash script, the first line (called a hash-bang or shebang) tells the computer what to run this file with (in this case, Bash).  
The rest of the file can be seen as being typed into the prompt automatically.  

To use this script, we must make it executable `chmod +x freqcount.sh`.  
Then we can simply `cat bash-ref.txt | ./freqcount.sh > freqcounted`  
`./` gives bash the path to the script to execute.  

For spell checking:  

```bash
cat bash-ref.txt | tr A-Z a-z | tr -cs a-z "\n" | sort | uniq | comm -23 - dict | less
```

# Globbing <a name = "globbing"></a>

`.` as a starting character has to be explicitly stated (?bashrc won't work)  
`*` represents any unspecified characters (down to 0 characters)  
`**` represents any characters, recursively (matches dir/dir/dir/dir/dir/dir/dir/file etc)  
`?` represents a single unspecific character  
`[ ]` characters in braces are specific  
`^` means logical NOT, e.g.: `[^a-c]*` matches any files that do not start with `a`, `b` or `c`  

# More utilities <a name = "more_utilities"></a>

> Every one was written first for a particular need, but untangled from the specific application.  

note: "file" here can usually mean stdin if no file has been specified as an argument.  
also: "print" here means output to stdout, which ends up on the terminal screen if you're not piping it to another program.  
and : Most of these utilities are not a direct feature of Bash, but are part of the same standard (POSIX).  

## String manipulation <a name = "string_manipulation"></a>
	grep (print all lines in given file that contain a match to the given pattern)
	head (print the first 10 lines of a file)
	tail (print the last 10 lines of a file)
	less (open a file in more and vi style read-only, space to page down (how to go up?), j and k for down and up scrolling, u and d for up and down jumping)
	tr (translates characters matching the first given pattern to the second pattern)
	strings (print printable strings of 4 or more characters from a file)
	sort (print sorted content of provided file(s))
	uniq (remove duplicate adjacent lines (consider sorting before this), outputs to stdout)
	wc (print newline, word, and byte counts for each file)
	sed (stream editor, learn regex)
	awk (pattern-action record manipulating program with its own language)
	nano (simple editor from GNU)
	vi (:q to exit, but if you're using it, come to our vim workshop instead)
	emacs (seriously, what are you doing here?)

## Administration <a name = "administration"></a>
	sudo (execute the following as root (requires password and the current user to be on the sudoer list))
	chmod (change file mode bits, e.g.: read, write, execute)
	top (displays a real time view of running processes ranked by cpu usage (highest usage at the top))
	whoami (prints current user's username)
	w (shows who's logged in, when they logged in, etc)
	id (shows given user's permissions (current user if none is given))
	uname -a (show kernel and os version, and more system information)

## File manipulation <a name = "file_manipulation"></a>
	ls (lists the contents of the current directory, -a to also list .files)
	find (search for files, example: `find . -iname "*bash*"`)
	realpath (shows the full path of a file)
	cp (copy file(s) to path)
	mv (moves file(s) to path)
	rm (removes file(s), FEAR ME!, NO UNDO!, -rf for directories)
	touch (updates last modified time, creates file if it doesn't exist)
	cat (concatenates file(s) to stdout)
	tac (concatenates file(s) to stdout in reverse order (last line first))
	mkdir (makes a directory)
	sha1sum (calculate the sha1 hash of a file sha256, 512, etc also possible)
	stat (displays file status, size, last modified, etc)
	unexpand (convert spaces to tabs)
	diff (compare files line by line)
	patch (apply a diff file)
	comm (outputs 3 columns: lines unique to file1, file2 and lines in common)

## Network management <a name = "network_management"></a>
	ip (network configuration tool, good source of info)(replaces ifconfig)

## Pipe manipulation <a name = "pipe_manipulation"></a>
	tee (write stdin to stdout and specified file(s))
	xargs (use the piped in text as arguments to a given command)
	pv (pipe viewer, meter data passing through a pipe)(not installed by default?)

## Environment <a name = "environment"></a>
	man (manual for given program)(man -a intro, OS introduction)
	info (modern alternative to man)
	whatis (search for a manual)
	apropos (search in manual pages)
	pwd (print working directory)
	time (times the following command)
	timeout (runs the following command with a specified timeout)
	sleep (do nothing for specified time)
	watch (run the following command every 2s (other times possible, -n))
	env (without arguments: prints the current environmental variables)
	history (shows command history)
	uptime (show machine uptime)
	lshw (list hardware information (also lsusb and lspci)
	free (shows free and used memory)

## Process manipulation <a name = "process_manipulation"></a>
	top (displays a real time view of running processes ranked by cpu usage (highest usage at the top))
	kill (terminate a process by process id)
	pgrep (find a process by name)
	pkill (terminate a process by name)

## Disk manipulation <a name = "disk_manipulation"></a>
	df (show amount disk free, try -h option for human readable)
	du (show amount disk used, try -h option for human readable)
	lsblk (list block devices)

Here be dragons!

	dd (manipulates raw data, can mess things up if not used carefully, read the manual (man dd))
	mount (attach a device's filesystem to the file tree)
	umount (unmount)


# Programs as arguments <a name = "programs_as_arguments"></a>

sudo, watch, time, xarg, etc  
for example: `time (sudo dmesg | tail)`  

# Job control <a name = "job_control"></a>

ctrl-z: temporarily suspends a "job" command and saves it in the background  
jobs: shows the jobs associated with the current session (does not include those of sub-shells)  
bg: shows the session's background jobs  
fg: bring a job to the foreground (defaults to %1)  
&: append to a command to start it in the background  
disown: remove a job from the current session without stopping it (won't receive SIGHUP)  
&!: start a job in the background and disown  
exec: start a program and close the current shell  

# Command substitution <a name = "command_substitution"></a>

Using the output of a command as a variable.

	$(command (sub-command))
	echo "$(sudo dmesg)" | tail

note: `$((expression))` is arithmetic expansion, it evaluates the expression and returns the result. (don't use `((` unless you want this)

# Loops and Conditionals <a name = "loops_and_conditionals"></a>

`;` is equivalent to a newline so the first loops example is equivalent to:  

```bash
for i in {0..4}
	do touch $i.test
done
```

Indentation does not matter in Bash but is done for readability.  

Loops:  
for loop, `for i in {0..4}; do touch $i.test; done`  
C style for loop if you prefer that.  
While loop, `while condition; do action; another action; done`  
and until, `until condition; do action; done` which is equivalent to `while !condition`.  

Conditional statements:  
such as `if condition; then action; fi` and `case <i'm not gonna go into this> esac`  

# Customisation <a name = "customisation"></a>

.bashrc  
aliases  

# Alternatives <a name = "alternatives"></a>

sh (usually the system default shell in POSIX mode)  
zsh (extended bourne shell with many extra features)  
not fish, anything but fish! (it's not POSIX compliant)  

most of the older shells have been superceded by Bash, though (d)ash survives for lightweight applications.  

# Package managers <a name = "package_managers"></a>

`apt` or the older `apt-get` for all debian based linux distributions (based on dpkg).  
`snap`, a very new package manager by canonical (makers of ubuntu) intended for all linux distros, with IoT and ease of use in mind. Has an "snap store" GUI available.  

`mac app store`, technically a package manager :P  
`homebrew`, free and open source package manager for macOS (collects user data, consider opting out with `brew analytics off`)  

`cygwin` a unix-like environment and command-line for windows, includes a package manager.  
`nuget` open source package manager for windows by microsoft.  

# References <a name = "references"></a>
https://www.gnu.org/software/bash/manual/bashref.html  
[bash-hackers.org](http://wiki.bash-hackers.org/) and their [list of tutorials](http://wiki.bash-hackers.org/scripting/tutoriallist)  
http://teohm.com/blog/shortcuts-to-move-faster-in-bash-command-line/  
[bash: an introduction to advanced usage (YT)](https://youtu.be/BJ0uHhBkzOQ)  
[readline: your other editor (YT)](https://youtu.be/MxRTh8wlmJk)  
https://en.wikipedia.org/wiki/List_of_Unix_commands
